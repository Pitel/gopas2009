package cz.gopas.kalkulacka

import android.app.Application
import android.util.Log
import androidx.annotation.IdRes
import androidx.core.content.edit
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.preference.PreferenceManager
import androidx.room.Room
import cz.gopas.kalkulacka.history.HistoryDatabase
import cz.gopas.kalkulacka.history.HistoryEntity
import kotlin.concurrent.thread

class CalcViewModel(app: Application) : AndroidViewModel(app) {
    private val prefs by lazy { PreferenceManager.getDefaultSharedPreferences(app) }
    private val db = Room.databaseBuilder(app, HistoryDatabase::class.java, "history")
        //.allowMainThreadQueries()
        .build()
        .historyDao()

    var ans: String?
        get() = prefs.getString(ANS_KEY, null)
        private set(value) = prefs.edit { putString(ANS_KEY, value) }

    private val _res = MutableLiveData<Float>()
    val res: LiveData<Float> = _res

    fun calc(a: Float, b: Float, @IdRes op: Int) {
        Log.d(TAG, "Calc!")
        thread {
            //Thread.sleep(5000)
            _res.postValue(
                when (op) {
                    R.id.add -> a + b
                    R.id.sub -> a - b
                    R.id.mul -> a * b
                    R.id.div -> a / b
                    else -> Float.NaN
                }.also {
                    ans = it.toString()
                    Log.d(TAG, "Calculated $it")
                    thread {
                        db.add(HistoryEntity(it))
                    }
                }
            )
        }
    }

    private companion object {
        private val TAG = CalcViewModel::class.simpleName
        private const val ANS_KEY = "ans"
    }

    override fun onCleared() {
        Log.d(TAG, "Cleared")
        super.onCleared()
    }
}