package cz.gopas.kalkulacka

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.onNavDestinationSelected
import cz.gopas.kalkulacka.history.HistoryFragment
import kotlinx.android.synthetic.main.fragment_calc.*

class CalcFragment : Fragment(R.layout.fragment_calc) {

    private val viewModel: CalcViewModel by viewModels()

    /*
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_calc, container, false)
     */

    init {
        setHasOptionsMenu(true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        calc.setOnClickListener { calc() }
        ops.setOnCheckedChangeListener { _, _ -> calc() }
        share.setOnClickListener {
            startActivity(
                Intent(Intent.ACTION_SEND)
                    .setType("text/plain")
                    .putExtra(Intent.EXTRA_TEXT, getString(R.string.share_text, res.text))
            )
        }
        ans.setOnClickListener {
            viewModel.ans?.let { ans -> b.editText?.setText(ans) }
        }

        hist.setOnClickListener {
            findNavController().navigate(R.id.historyAction)
        }
        findNavController().currentBackStackEntry?.savedStateHandle?.getLiveData<Float>(
            HistoryFragment.HISTORY_KEY
        )?.observe(viewLifecycleOwner) {
            a.editText?.setText(it.toString())
        }

        viewModel.res.observe(viewLifecycleOwner) {
            Log.d(TAG, "Got result $it")
            res.text = it.toString()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem) =
        item.onNavDestinationSelected(findNavController()) || super.onOptionsItemSelected(item)

    private fun calc() {
        Log.d(TAG, "Calc!")
        a.editText?.clearFocus()
        b.editText?.clearFocus()
        val a = a.editText?.text.toString().toFloatOrNull() ?: 0f
        val b = b.editText?.text.toString().toFloatOrNull() ?: 0f
        val op = ops.checkedRadioButtonId
        if (op == R.id.div && b == 0f) {
            findNavController().navigate(R.id.zeroAction)
        } else {
            viewModel.calc(a, b, op)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        Log.d(TAG, "View destroyed")
    }

    private companion object {
        private val TAG = CalcFragment::class.simpleName
    }
}