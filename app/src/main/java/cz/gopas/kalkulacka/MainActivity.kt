package cz.gopas.kalkulacka

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI.setupActionBarWithNavController

class MainActivity : AppCompatActivity() {

    init {
        FragmentManager.enableDebugLogging(true)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        /*
        if (savedInstanceState == null) {
            supportFragmentManager.commit {
                replace(android.R.id.content, CalcFragment())
            }
        }
         */

        val navHostFragment = supportFragmentManager.findFragmentById(R.id.nav) as NavHostFragment
        val navController = navHostFragment.navController
        setupActionBarWithNavController(this, navController)

        Toast.makeText(this, "Create", Toast.LENGTH_SHORT).show()
        Log.d(TAG, "Create")

        Log.d(TAG, "Intent: ${intent.dataString}")
    }

    override fun onSupportNavigateUp() = findNavController(R.id.nav).navigateUp() || super.onSupportNavigateUp()

    /*
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString(BUNDLE_KEY, res.text.toString())
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        res.text = savedInstanceState.getString(BUNDLE_KEY)
        //Log.d(TAG, "${savedInstanceState.keySet().toList()}")
    }
     */

    override fun onResume() {
        super.onResume()
        Toast.makeText(this, "Resume", Toast.LENGTH_SHORT).show()
        Log.d(TAG, "Resume")
    }

    override fun onStop() {
        super.onStop()
        Toast.makeText(this, "Stop", Toast.LENGTH_SHORT).show()
        Log.d(TAG, "Stop")
    }

    private companion object {
        private val TAG : String = MainActivity::class.simpleName!!
        private const val BUNDLE_KEY = "res"
    }
}