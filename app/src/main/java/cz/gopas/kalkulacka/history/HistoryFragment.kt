package cz.gopas.kalkulacka.history

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import cz.gopas.kalkulacka.R
import kotlinx.android.synthetic.main.fragment_history.*
import kotlin.concurrent.thread
import kotlin.random.Random

class HistoryFragment : Fragment(R.layout.fragment_history) {

    private val viewModel: HistoryViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val historyAdapter = HistoryAdapter {
            with(findNavController()) {
                previousBackStackEntry?.savedStateHandle?.set(HISTORY_KEY, it)
                popBackStack()
            }
        }
        with(recycler) {
            setHasFixedSize(true)
            adapter = historyAdapter
        }

        viewModel.db.getAll().observe(viewLifecycleOwner) { historyAdapter.submitList(it) }

        /*
        thread {
            viewModel.db.add(HistoryEntity(Random.nextFloat()))
        }
         */
    }

    companion object {
        const val HISTORY_KEY = "history"
    }
}